import os


class Config(object):
    pass


class DevConfig(Config):
    DEBUG = True
    ENV = 'development'
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://{0}:{1}@{2}.{3}.svc:3306/{4}".format(os.environ.get("DATABASE_USER"),
                                                                                    os.environ.get("DATABASE_PASSWORD"),
                                                                                    os.environ.get(
                                                                                        "DATABASE_SERVICE_NAME"),
                                                                                    os.environ.get(
                                                                                        "OPENSHIFT_BUILD_NAMESPACE"),
                                                                                    os.environ.get("DATABASE_NAME"))
    GOOGLE_OAUTH2_CLIENT_ID = os.environ.get("GOOGLE_OAUTH2_CLIENT_ID")
    GOOGLE_OAUTH2_CLIENT_SECRET = os.environ.get("GOOGLE_OAUTH2_CLIENT_SECRET")
    SECRET_KEY = os.environ.get("FLASK_SECRET_KEY")
