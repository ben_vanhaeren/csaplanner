import os
import logging
from webapp import create_app

env = os.environ.get('WEBAPP_ENV','dev')
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
application = create_app('webapp.config.{}Config'.format(env.capitalize()))

if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    application.logger.handlers = gunicorn_logger.handlers
    application.logger.setLevel(gunicorn_logger.level)

else :
    application.run(host='0.0.0.0', port=8080)
